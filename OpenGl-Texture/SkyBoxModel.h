#ifndef SKYBOXMODEL_H
#define SKYBOXMODEL_H

#include "Window.h"
#include "Model.h"
#include <array>

class SkyBoxModel : public Model
{
private: 
	unsigned int VAO;
	unsigned int VBO;
	std::vector<std::string> faces;

	unsigned int cubemapTexture;

	//Cube 36 points 3 vertices er point
	std::array<float,36*3> vertices;

public:
	SkyBoxModel();
	virtual ~SkyBoxModel();
 
	virtual void Draw() override; 

private: 
	void Load(); 
	virtual void Init() override; 

	unsigned int loadCubemap();
};

#endif // !SKYBOXMODEL_H
