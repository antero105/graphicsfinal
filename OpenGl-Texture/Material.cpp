#include "Material.h"

Material::Material()
{ 
}

Material::Material(vec3 Ambient, vec3 Diffuse, vec3 Spec)
	:ambient(Ambient)
	,diffuse(Diffuse)
	,specular(Spec)
{ 
}


Material::~Material()
{
}


void Material::loadMaterial(std::string name, std::vector<Vertex>& vertices, int offset)
{ 
	FILE * fPtr = fopen("../resources/objs/house.mtl", "r");

	if (fPtr == NULL)
	{ 
		return;
	}
	//Entire file line
	char lineheader[128];

	int res = fscanf(fPtr, "%s", lineheader);

	while (res != EOF)
	{ 
		//New Material
		if (strcmp(lineheader, "newmtl") == 0)
		{
			char  mtlName[80];
			fscanf(fPtr, "%s\n", mtlName);

			//material name = name passed in
			if ((std::string)mtlName == name)
			{
				//Check Properties
				char  prop[80];
				fscanf(fPtr, "%s\n", prop); 

				//ambient
				if ((std::string)prop == "Ka")
				{
					setAmbient(fPtr, vertices, offset);
				}

				//diffuse
				if ((std::string)prop == "Kd")
				{
					setDiffuse(fPtr, vertices, offset);
				}

				//specular
				if ((std::string)prop == "Ks")
				{
					setSpecular(fPtr, vertices, offset);
				}

				char path[80];
				fscanf(fPtr, "%s\n", path); 
				//Checks if there is a material
				if ((std::string) path == "map_Kd")
				{
					fscanf(fPtr, "%s\n", path);
					LoadTexture(path);
				}
				break;
			}
		}
	}
}

void Material::setDiffuse(FILE* fPtr, std::vector<Vertex>& vertices, int offset)
{
	float  r, g, b;
	fscanf(fPtr, "%f" "%f" "%f\n", &r, &g, &b);

	for (int i = 0; i < offset; i++)
	{
		vertices.at(i).Normal.r = r;
		vertices.at(i).Normal.g = g;
		vertices.at(i).Normal.b = b;
		//	diffuse.r = r;
		//	diffuse.g = g;
		//	diffuse.b = b;
	}
}

void Material::setAmbient(FILE* fPtr, std::vector<Vertex>& vertices, int offset)
{
	//I Don't Know the Format
}

void Material::setSpecular(FILE* fPtr, std::vector<Vertex>& vertices, int offset)
{
	//I Don't Know the Format 
}

void Material::LoadTexture(char* path)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);    // set texture wrapping to GL_REPEAT (default wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	// The FileSystem::getPath(...) is part of the GitHub repository so we can find files on any IDE/platform; replace it with your own image path.
	unsigned char *data = stbi_load(path, &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);
}
