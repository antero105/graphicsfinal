#ifndef MESH_H
#define MESH_H

#include "Material.h"
#include "Vertex.h"
#include "Transform.h"

#include <vector>
#include <string>

struct Texture {
    unsigned int id;
    std::string type;
    std::string path;
};

class Mesh
{
private: 
	void Scale(Shader shader, glm::vec3 size) const;
	void Move(Shader shader, glm::vec3 position) const;
public:

	Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);

	std::vector<Vertex> vertices;
	std::vector<Vertex> normals;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	void Draw(Shader const& shader, Transform* transform) const;

	//void setMaterial(Material* mat) { material = mat; }

private:

	unsigned int VAO, VBO, EBO;

	void SetupMesh(); 
};


#endif //!MESH_H