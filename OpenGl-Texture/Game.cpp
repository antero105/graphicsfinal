#include "Camera.h"
#include "Game.h"
#include "SceneRenderer.h"
#include "SkyBoxModel.h"
#include "Score.h"
#include "Text.h"

void Game::init()
{
	//init player score
	Score::instance()->initialize();

	// glfw window creation
	window = new Window(SCR_WIDTH, SCR_HEIGHT, 3, 3);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
	}

	window->GLInit();

	Camera* camera = new Camera();
	camera->transform->setPosition(glm::vec3(0.0f, 0.3f, 4.0f));
	camera->transform->setSize(glm::vec3(1.0f, 1.0f, 1.0f));
	window->setCamera(*camera);
	window->activateCamera();

	//Load MOdels
	Model* city = new Model(*"../resources/objs/City/City.obj");
	city->transform->setPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	city->tag = "city";
	
	Model* nanoSuit = new Model(*"../resources/objs/nano/NanoSuit.obj");
	nanoSuit->transform->setPosition(glm::vec3(1.2f, 0.0f, 3.2f));
	nanoSuit->transform->setSize(glm::vec3(1.3f, 0.9f, 0.9f));
	nanoSuit->tag = "Enemy";

	Model* coin1 = new Model(*"../resources/objs/coin/Coin.obj");
	coin1->transform->setPosition(glm::vec3(-1.2f, 0.0f, -2.0f));
	coin1->transform->setSize(glm::vec3(1.2f, 0.9f, 0.9f));
	coin1->tag = "Coin";

	Model* coin2 = new Model(*"../resources/objs/coin/Coin.obj");
	coin2->transform->setPosition(glm::vec3(2.1f, 0.0f, 1.5f));
	coin2->transform->setSize(glm::vec3(1.2f, 0.9f, 0.9f));
	coin2->tag = "Coin";

	Model* coin3 = new Model(*"../resources/objs/coin/Coin.obj");
	coin3->transform->setPosition(glm::vec3(1.8f, 0.0f, 2.0f));
	coin3->transform->setSize(glm::vec3(1.2f, 0.9f, 0.9f));
	coin3->tag = "Coin";

	Model* coin4 = new Model(*"../resources/objs/coin/Coin.obj");
	coin4->transform->setPosition(glm::vec3(-1.2f, 0.0f, 2.0f));
	coin4->transform->setSize(glm::vec3(1.2f, 0.9f, 0.9f));
	coin4->tag = "Coin";

	Model* coin5 = new Model(*"../resources/objs/coin/Coin.obj");
	coin5->transform->setPosition(glm::vec3(-2.2f, 0.0f, 2.0f));
	coin5->transform->setSize(glm::vec3(1.2f, 0.9f, 0.9f));
	coin5->tag = "Coin";
	
	SkyBoxModel* skyBox = new SkyBoxModel();
	skyBox->transform->setPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	skyBox->tag = "Enviroment";

	Text* timer = new Text("Timer", glm::vec3(580.0f, 545.0f, 0.5f), 1, glm::vec3(0.3, 0.7f, 0.9f));
	timer->tag = "Timer";
	SceneRenderer::Instance().addModel(skyBox);
	SceneRenderer::Instance().addModel(city);
	SceneRenderer::Instance().addModel(nanoSuit);
	SceneRenderer::Instance().addModel(coin1);
	SceneRenderer::Instance().addModel(coin2);
	SceneRenderer::Instance().addModel(coin3);
	SceneRenderer::Instance().addModel(coin4);
	SceneRenderer::Instance().addModel(coin5);
	SceneRenderer::Instance().addModel(timer);
}

void Game::Update()
{

	glEnable(GL_DEPTH_TEST);
	// render loop
	while (!glfwWindowShouldClose(window->window.get()))
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		SceneRenderer::Instance().Render();
		window->Update();
		window->DoCollisions(SceneRenderer::Instance().getModels());
	}
	Score::instance()->ReWriteScoreTxt();
	// glfw: terminate, clearing all previously allocated GLFW resources.
	glfwTerminate();
}

Game::Game()
{
}


Game::~Game()
{
	delete window;
}
