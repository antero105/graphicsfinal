#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream> 
#include "Window.h"

class Game
{
private:
	// settings
	const unsigned int SCR_WIDTH = 800;
	const unsigned int SCR_HEIGHT = 600;
	Window* window;
public:
	void init();
	void Update();
	Game();
	~Game();
};

