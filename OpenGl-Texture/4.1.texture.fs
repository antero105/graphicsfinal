#version 330 core
out vec4 FragColor;

struct Light {
    vec3 position;  
    vec3 direction;
    float cutOff;
    float outerCutOff;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	
    float constant;
    float linear;
    float quadratic;
};

in vec3 FragPos;  
in vec3 Normal;  
in vec2 TexCoord;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_normal1;
  
uniform vec3 viewPos;
uniform Light light;

uniform float transparency = 1.0;

void main()
{ 

	vec4 texColor = texture(texture_normal1, TexCoord);
	if(texColor.a < 0.1)
	{ 
		discard;
	}

    vec3 lightDir = normalize(light.position - FragPos);
    
    // ambient
    vec3 ambient = light.ambient * texture(texture_normal1, TexCoord).rgb;
    
    // diffuse 
    vec3 norm = normalize(Normal);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(texture_diffuse1, TexCoord).rgb;  
    
    // specular
   
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);  
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 10.0);
	vec3 specular = vec3(0.2) * spec;
	//spotlight
    // check if lighting is inside the spotlight cone
    
    // attenuation
    float distance    = length(light.position - FragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
	

    ambient  *= attenuation; // remove attenuation from ambient, as otherwise at large distances the light would be darker inside than outside the spotlight due the ambient term in the else branche
    diffuse   *= attenuation;
    specular *= attenuation;   
        
    vec3 result = ambient + diffuse + specular;

	FragColor = vec4(result, transparency);
} 