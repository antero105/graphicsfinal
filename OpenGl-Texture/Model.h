#ifndef MODEL_H
#define MODEL_H

#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "Mesh.h"
#include "Material.h" 
#include "Transform.h"
#include <vector> 
#include <string>



class Model
{
public:
	Model(const char &path);
	Model(const char &path, Shader& shade);
	Model(const char &path, const char *vs, const char *fs);
	Model(const char *vs, const char *fs);
	
	virtual ~Model();

	virtual void Draw(); 

	bool isActive() { return active; };

	void setActive(bool newActive) { active = newActive; };

public:
	std::vector< unsigned int > vertexIndices, uvIndices, normalIndices, indices; 
	std::vector<Texture> loaded_textures;

	std::string directory;

	std::string tag;

	Transform* transform;
private: 
	virtual void Init();
 
	Mesh ProcessMesh(aiMesh *mesh, const aiScene *scene);
	void ProcessNode(aiNode *node, const aiScene *scene);
	void LoadModel(std::string const &path);
	
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);

	unsigned int TextureFromFile(const char *path, const std::string &directory);

	bool active;

private: 
	Shader shader;

	std::vector<Mesh> meshes; 
	

public:
	const Shader& getShader() const { return shader; }

	std::vector<Mesh> const& getMeshes() const { return meshes; } 

	void UpdateSpotLight();
};

#endif // !MODEL
