#ifndef TRANSPARENTMODEL_H
#define TRANSPARENTMODEL_H

#include "Model.h"

class TransparentModel : public Model
{
private:
	//Default Transparency
	float transparency = 0.5f;

public:
	TransparentModel(const char &path);
	TransparentModel(const char &path, float Transparency);
	TransparentModel(const char &path, Shader& shade);
	TransparentModel(const char &path, Shader& shade, float Transparency);
	TransparentModel(const char &path, const char *vs, const char *fs); 
	TransparentModel(const char &path, const char *vs, const char *fs, float Transparency);

	virtual ~TransparentModel();

	virtual void Draw() override; 

	void setTransparency(float Transparency) { transparency = Transparency; }
};

#endif // !TRANSPARENTMODEL_H


