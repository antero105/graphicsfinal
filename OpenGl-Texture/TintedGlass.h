#ifndef TINTEDGLASS_H 
#define TINTEDGLASS_H 

#include "TransparentModel.h"

class TintedGlass : public TransparentModel
{
public:
	TintedGlass(const char& path);
	virtual ~TintedGlass();
};

#endif // !TINTEDGLASS_H
