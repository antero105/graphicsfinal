#include "Camera.h"

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

void Camera::ProcessKey(Camera_Dir direction, float deltatime)
{
	float velocity = MovementSpeed * deltatime;

	if (direction == FORWARD) 
	{
		transform->setPosition(transform->getPosition() += cameraTgt * velocity);
	//	cameraTgt += cameraTgt * velocity;
	}
	if (direction == BACKWARD) 
	{
		transform->setPosition(transform->getPosition() -= cameraTgt * velocity);
	}
	if (direction == LEFT) 
	{
		transform->setPosition(transform->getPosition() -= Right * velocity);
	}
	if (direction == RIGHT) 
	{
		transform->setPosition(transform->getPosition() += Right * velocity);
	}
	if (direction == UP) 
	{
		transform->setPosition(transform->getPosition() += upVector * velocity);
	}
	if (direction == DOWN) 
	{
		transform->setPosition(transform->getPosition() -= upVector * velocity);
	}
}

mat4 Camera::getProjectionMatrix() const
{
	return glm::perspective(
		glm::radians((float)Zoom),
		3.0f,
		0.1f,
		100.0f);
}


Camera::Camera()
	: spotLightShader("4.1.SpotLight.vs", "4.1.SpotLight.fs"), transform(new Transform())
{
	transform->setPosition(vec3(0.0f, 0.0f, 3.0f));
	cameraTgt = vec3(0.0f, 0.0f, -1.0f);
	upVector = vec3(0.0f, 1.0f, 0.0f);
	cameraDir = glm::normalize(transform->getPosition() - cameraTgt);
	Right = glm::normalize(glm::cross(upVector, cameraDir));
	cameraUp = glm::cross(cameraDir, Right);
}

Camera::~Camera()
{
}

void Camera::Update()
{
	glm::vec3 front;
	front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	front.y = 0.0f;
	front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	cameraTgt = glm::normalize(front);

	Right = glm::normalize(glm::cross(cameraTgt, upVector));  
	cameraUp = glm::normalize(glm::cross(Right, cameraTgt)); 
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
{
	xoffset *= MouseSensitivity;
	yoffset *= MouseSensitivity;

	Yaw += xoffset;
	Pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch)
	{
		if (Pitch > 89.0f)
		{
			Pitch = 89.0f;
		}
		if (Pitch < -89.0f) 
		{
			Pitch = -89.0f;
		}
	}

	Update();
}


