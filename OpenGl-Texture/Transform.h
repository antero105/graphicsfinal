#pragma once
#include <glm/glm.hpp>
class Transform
{
public:
	glm::vec3 position;
	glm::vec3 size;
public:
	Transform() : position(glm::vec3(0.0f,0.0f,0.0f)), size(glm::vec3(0.2f, 0.2f, 0.2f)) { };
	~Transform();
	void setPosition(glm::vec3 newPosition) { position = newPosition; };
	glm::vec3 getPosition() { return position; };
	void setSize(glm::vec3 newSize) { size = newSize; };
	glm::vec3 getSize() { return size; };
};

