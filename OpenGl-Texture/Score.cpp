#include "Score.h"
#include<iostream>
#include<fstream>

Score::Score()
{
}


Score::~Score()
{
}

void Score::ReadScoreTxt(const std::string & path)
{
	std::ifstream src(path);
	if (!src)
	{
		std::cout << "Error openning score file" << std::endl;
		return;
	}
	_scoreBoard.clear();
	std::string line;
	while (getline(src, line))
	{
		std::pair<std::string, int> temp = Split(line, ',');
		if (temp.first != "") {
			_scoreBoard.push_back(std::make_pair(temp.first, temp.second));

			//for testing
			std::cout << temp.first << temp.second << std::endl;
		}
	}
}

void Score::WriteScoreTxt(const std::string & path)
{
	std::ofstream write(path, std::ios_base::app | std::ios_base::out);
	write << _playerName + "," + std::to_string(_playerScore) << std::endl;
}

void Score::ReWriteScoreTxt(const std::string & path)
{
	ReadScoreTxt();
	std::ofstream ofs;
	ofs.open(path, std::ofstream::out | std::ofstream::trunc);

	for (auto it : _scoreBoard)
	{
		std::ofstream write(path, std::ios_base::app | std::ios_base::out);
		write << it.first + "," + std::to_string(it.second) << std::endl;
	}
	ofs.close();

}

std::pair<std::string, int> Score::Split(const std::string & text, char sep)
{
	std::pair<std::string, int> words;
	std::size_t start = 0, end = 0;
	while ((end = text.find(sep, start)) != std::string::npos) {
		std::string word = text.substr(start, end - start);
		words.first = word;
		start = end + 1;
	}
	words.second = stoi(text.substr(start));
	return words;
}
