#ifndef WINDOW_H
#define WINDOW_H
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "Camera.h"

#include <iostream>
#include <string>
#include <list>
#include "Model.h"
#include "Vertex.h"
#include "Transform.h"

struct DestroyglfwWin
{
	void operator()(GLFWwindow* ptr) {
		glfwDestroyWindow(ptr);
	}
};

typedef std::unique_ptr<GLFWwindow, DestroyglfwWin> SmartGLFWwindow;

class Window
{
public:
	Window(int Width, int Height, int GLMajor, int GLMinor);
	~Window();

	SmartGLFWwindow window;

	void GLInit();
	void Update();
	void DoCollisions(std::list<Model*> models);
	bool CheckCollision(Transform* one, Transform* two);
private:
	//Window Size
	static	int width;
	static	int height;
	bool menuActive = false;

	//GL Version
	int major;
	int minor;

	double deltaTime = 0.0f;
	double currentTime = 0.0f;
	double lastTime = 0.0f;

	bool firstMouse = true;
	double lastX;
	double lastY;
	clock_t clock_t;

public:
	void setCamera(Camera &camera);
	void activateCamera();
	static Camera* currentCamera;

public:
	static int getWidth() { return width; }
	static int getHeight() { return height; }
	void showPlayersTable();

private:
	static void mouse_button_cb(GLFWwindow* window, int button, int action, int mods);
	static void keyboard_cb(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
	static void mouse_callback(GLFWwindow* window, double xpos, double ypos);

	void mouseClick(int button, int action, int mods);
	void keyPress(int key, int scancode, int action, int mods);
	void mouseScroll(double xoffset, double yoffset);
	void mouseMove(double xpos, double ypos);

	Camera* camera = nullptr;
};

#endif // !WINDOW_H


