#include "TransparentModel.h" 
#include "Window.h"


TransparentModel::TransparentModel(const char & path)
	:Model(path)
{
}

TransparentModel::TransparentModel(const char & path, float Transparency)
	:Model(path)
	,transparency(Transparency)
{
}

TransparentModel::TransparentModel(const char & path, Shader & shade)
	:Model(path, shade)
{
}

TransparentModel::TransparentModel(const char & path, Shader & shade, float Transparency)
	:Model(path, shade)
	,transparency(Transparency) 
{
}

TransparentModel::TransparentModel(const char & path, const char * vs, const char * fs)
	:Model(path, vs, fs)
{
}

TransparentModel::TransparentModel(const char & path, const char * vs, const char * fs, float Transparency)
	:Model(path, vs, fs)
	,transparency(Transparency) 
{
}

TransparentModel::~TransparentModel()
{
}

void TransparentModel::Draw()
{ 
	Model::UpdateSpotLight();
	auto& meshes = Model::getMeshes();
	auto& shader = Model::getShader();

	shader.setFloat("transparency", transparency);

	glm::mat4 projection = glm::perspective(
		glm::radians((float)Window::currentCamera->getZoom())
		, (float)Window::getWidth() / (float)Window::getHeight()
		, 0.1f
		, 100.0f
	);

	glm::mat4 view = glm::mat4(
		glm::mat3(Window::currentCamera->getVieMatrix())
	); // remove translation from the view matrix

	shader.setMat4("view", view);
	shader.setMat4("projection", projection);
	auto it = meshes.begin();
	for (; it != meshes.end(); ++it)
	{
		it->Draw(shader, transform);
	}
}
