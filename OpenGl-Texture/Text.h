#pragma once
#include <iostream>
#include <map>
#include "Model.h"
class Text: public Model
{
private:
	/// Holds all state information relevant to a character as loaded using FreeType
	struct Character {
		GLuint TextureID;   // ID handle of the glyph texture
		glm::ivec2 Size;    // Size of glyph
		glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
		GLuint Advance;    // Horizontal offset to advance to next glyph
	};
	GLuint VAO, VBO;
	const GLuint WIDTH = 800, HEIGHT = 600;
	std::map<GLchar, Character> Characters;
	std::string textValue;
	glm::vec3 color;
	float scale;
public:
	Text(std::string textValue, glm::vec3 position, float scale, glm::vec3 color);
	~Text();
	void Render(std::string text, float x, float y, float scale, glm::vec3 color);
	void Draw() override;
	void setText(std::string text) { textValue = text; };
};

