#include "SceneRenderer.h"
#include <algorithm>
void SceneRenderer::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (Model* m : models)
	{
		if (m->isActive())
		{
			m->Draw();
		}
	}
}