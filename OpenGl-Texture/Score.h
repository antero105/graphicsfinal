#pragma once
#include <string>
#include <map>
#include <list>
#include <iostream>

class Score
{
private:
	std::string _playerName;
	int _playerScore;
	std::list<std::pair<std::string, int>> _scoreBoard;

	std::pair<std::string, int> Split(const std::string & text, char sep);

public:
	Score();
	~Score();

	static Score* instance()
	{
		static Score* s{ new Score };
		return s;
	}

	inline void AddScore(int increment) {
		for (std::pair<std::string, int >& it : _scoreBoard)
		{
			if (it.first == _playerName) {
				it.second += increment;
			}
		}
	}

	inline void initialize() {
		std::string name;
		std::cout << "Please enter player name" << std::endl;
		std::cin >> name;
		_playerName = name;
		_scoreBoard.push_back(std::make_pair(name, 0));
	}

	void ReadScoreTxt(const std::string& path = "../resources/Score.txt");
	void WriteScoreTxt(const std::string& path = "../resources/Score.txt");
	void ReWriteScoreTxt(const std::string& path = "../resources/Score.txt");

	const std::string& GetPlayerName() const { return _playerName; };
	int GetPlayerScore() const { return _playerScore; };
	void SetPlayerScore(int score) { _playerScore = score; };
	const std::list<std::pair<std::string, int>>& GetScoreBoard() const { return _scoreBoard; };


	//some combined inline methods for conviniency
	inline const std::list<std::pair<std::string, int>>& ReadTxtAndGetScoreBoard(const std::string& path = "../resources/Score.txt")
	{
		ReadScoreTxt();
		return GetScoreBoard();
	}
	inline void SetScoreAndWriteTxt(int score, const std::string& path = "../resources/Score.txt")
	{
		SetPlayerScore(score);
		WriteScoreTxt(path);
	}
};

