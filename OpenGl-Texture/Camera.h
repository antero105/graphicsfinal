#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image/stb_image.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include "learnopengl/shader.h"

#include "Transform.h"

enum Camera_Dir
{
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Camera
{
private:
	//Position, Target, upVector
	vec3 cameraTgt;
	vec3 cameraDir;
	vec3 upVector;
	vec3 Right;
	vec3 cameraUp;

	float Zoom = 45.0;
	float MovementSpeed = 2.5f;
	float MouseSensitivity = 0.1f;

	float Pitch = 0.0f;
	float Yaw = -90.0f;

	Shader spotLightShader;

public:
	void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);
	void ProcessKey(Camera_Dir direction, float deltatime); 
	Transform* transform;
	mat4 getVieMatrix() const { return glm::lookAt(transform->getPosition(), transform->getPosition() + cameraTgt, upVector); };
	mat4 getProjectionMatrix() const;
	vec3 const& getTarget() const { return cameraTgt; };
	double const& getZoom() const { return Zoom; }

	void adjustZoom(double z) { Zoom += z; } 
	void setZoom(double z) { Zoom = z; }

public:
	Camera();
	~Camera();

	void Update();
};

#endif // !CAMERA_H
