#ifndef SCENERENDERER_H
#define SCENERENDERER_H

#include "Model.h"
#include "Text.h"
#include <list>

class SceneRenderer
{
public:
	static SceneRenderer& Instance()
	{
		static SceneRenderer instance;
		return instance;
	}

	void addModel(Model* model) { models.push_back(model); }
	
	std::list<Model*> getModels() { return models; };
	void Render();

private: 
	std::list<Model*> models;

private:
	inline explicit SceneRenderer()
	{
	}

	inline ~SceneRenderer()
	{
	}

	inline explicit SceneRenderer(SceneRenderer const&)
	{
	}

	inline SceneRenderer& operator=(SceneRenderer const&)
	{
		return *this;
	}
};

#endif // !SCENERENDERER_H
