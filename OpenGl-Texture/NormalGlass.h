#ifndef NORMALGLASS_H
#define NORMALGLASS_H

#include "TransparentModel.h"

class NormalGlass : public TransparentModel
{
public:
	NormalGlass(const char& path);
	virtual ~NormalGlass();
};

#endif // !NORMALGLASS_H
