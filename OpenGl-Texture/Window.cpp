#include <algorithm>
#include <iostream>
#include <string>
#include <cmath>
#include "Window.h"
#include "glm/ext.hpp"
#include "Score.h"
#include "Text.h"
#include "SceneRenderer.h"


Camera* Window::currentCamera;

int Window::width;
int Window::height;

Window::Window(int Width, int Height, int GLMajor, int GLMinor)
	: major(GLMajor)
	, minor(GLMinor)
{ 
	width = Width;
	height = Height;
	clock_t = clock();
}

Window::~Window()
{
}

void Window::Update()
{
	currentTime = glfwGetTime();
	deltaTime = (currentTime - lastTime);
	lastTime = currentTime;
	for (Model* model : SceneRenderer::Instance().getModels())
	{
		if (model->tag == "Timer")
		{
			Text* text = (Text*) model;
			text->setText(std::to_string(clock() /1000));
		}
	}
	glfwSwapBuffers(window.get());
	glfwPollEvents();
}

void Window::DoCollisions(std::list<Model*> models)
{
	std::for_each(models.begin(), models.end(), [this](Model* model) {
		if (model->tag == "Coin" && CheckCollision(currentCamera->transform, model->transform))
		{
			std::cout << "Collides" << std::endl;
			model->setActive(false);
			SceneRenderer::Instance().getModels().remove(model);
			Score::instance()->AddScore(5);
		}
	});
}

bool Window::CheckCollision(Transform* one, Transform* two)
{
	return glm::distance(one->getPosition(), two->getPosition()) >= 8.8f;
 }

void Window::GLInit()
{
	// glfw: initialize and configure
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//Creating the window
	window = std::unique_ptr<GLFWwindow, DestroyglfwWin>(glfwCreateWindow(width, height, "FinalProject", nullptr, nullptr));

	// have the glfwwindow know about custom window class

	glfwSetInputMode(window.get(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// have the glfwwindow know about custom window class
	glfwSetMouseButtonCallback(window.get(), Window::mouse_button_cb);
	glfwSetWindowUserPointer(window.get(), this);
	glfwSetKeyCallback(window.get(), Window::keyboard_cb);
	glfwSetScrollCallback(window.get(), Window::scroll_callback);
	glfwSetCursorPosCallback(window.get(), Window::mouse_callback);


	glfwMakeContextCurrent(window.get());
#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

	// glad: load all OpenGL function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return;
	}
}


//calls the keypress method
void Window::keyboard_cb(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	auto win = static_cast<Window*>(glfwGetWindowUserPointer(window));
	win->keyPress(key, scancode, action, mods);
}

//checks key press and calls correct method
void Window::keyPress(int key, int scancode, int action, int mods)
{
	if (glfwGetKey(window.get(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window.get(), true);
	}
	if (glfwGetKey(window.get(), GLFW_KEY_W) == GLFW_PRESS)
	{
		camera->ProcessKey(FORWARD, deltaTime);
	}
	if (glfwGetKey(window.get(), GLFW_KEY_S) == GLFW_PRESS)
	{
		camera->ProcessKey(BACKWARD, deltaTime);
	}
	if (glfwGetKey(window.get(), GLFW_KEY_A) == GLFW_PRESS)
	{
		camera->ProcessKey(LEFT, deltaTime);
	}
	if (glfwGetKey(window.get(), GLFW_KEY_D) == GLFW_PRESS)
	{
		camera->ProcessKey(RIGHT, deltaTime);
	}
	if (glfwGetKey(window.get(), GLFW_KEY_UP) == GLFW_PRESS)
	{
		camera->ProcessKey(UP, deltaTime);
	}
	if (glfwGetKey(window.get(), GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		camera->ProcessKey(DOWN, deltaTime);
	}
	if (glfwGetKey(window.get(), GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		showPlayersTable();

	}


}

//check the scroll offset
void Window::mouseScroll(double xoffset, double yoffset)
{
	if (camera->getZoom() >= 1.0f && camera->getZoom() <= 45.0f)
	{
		camera->adjustZoom(-yoffset);
	}
	if (camera->getZoom() <= 1.0f)
	{
		camera->setZoom(1.0f);
	}
	if (camera->getZoom() >= 45.0f)
	{
		camera->setZoom(45.0f);
	}
}

//mouse movement callback
void Window::mouseMove(double xpos, double ypos)
{
	if (firstMouse)  // the position when the application starts
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera->ProcessMouseMovement(xoffset, yoffset);
}

void Window::setCamera(Camera & camera)
{
	this->camera = &camera;
}

void Window::activateCamera()
{
	currentCamera = camera;
}

void Window::showPlayersTable()
{
	
	if (!menuActive)
	{
		// desactivate everything
		for (Model* model : SceneRenderer::Instance().getModels())
		{
			model->setActive(false);
		}
		std::list < std::pair < std::string, int>> player_list = Score::instance()->ReadTxtAndGetScoreBoard();
		int count = 0;
		for (std::pair <std::string, int> player : player_list)
		{
			std::string text = std::to_string(count + 1) + "- " + player.first + ", Score: " + std::to_string(player.second);
			Text* renderText = new Text(text, glm::vec3(0.0f, 570.0f - (30 * count), 0.0f), 0.5f, glm::vec3(1.0f, 3.0f, 1.0f));
			renderText->tag = "List";
			SceneRenderer::Instance().addModel(renderText);
			count += 1;
		}
	}
	else 
	{
		for (Model* model : SceneRenderer::Instance().getModels())
		{
			if (model->tag == "List")
			{
				model->setActive(false);
				SceneRenderer::Instance().getModels().remove(model);

			}
			else {
				model->setActive(true);
			}
		}
	}
	menuActive = !menuActive;
}

//mouse button call back
void Window::mouse_button_cb(GLFWwindow * window, int button, int action, int mods)
{
	auto win = static_cast<Window*>(glfwGetWindowUserPointer(window));
	win->mouseClick(button, action, mods);
}

//mouse click method
void Window::mouseClick(int button, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		double xpos, ypos;
		glfwGetCursorPos(window.get(), &xpos, &ypos);
		std::cout << "xpos: " << xpos << " ypos: " << ypos << std::endl;

		// place triangle at position
		float ixpos = static_cast<float>(xpos);
		float iypos = static_cast<float>(ypos);

		Vertex v;
		v.Position.x = ixpos;
		v.Position.y = iypos;
		v.Position.y = iypos;
		v.Position.z = 0;
	}
}

//scroll call back
void Window::scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	auto win = static_cast<Window*>(glfwGetWindowUserPointer(window));
	win->mouseScroll(xoffset, yoffset);
}

//mouse call back
void Window::mouse_callback(GLFWwindow * window, double xpos, double ypos)
{
	auto win = static_cast<Window*>(glfwGetWindowUserPointer(window));
	win->mouseMove(xpos, ypos);
}

 
