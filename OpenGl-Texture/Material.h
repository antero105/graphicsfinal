#ifndef MATERIAL_H
#define MATERIAL_H

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <learnopengl/shader_m.h> 
#include <stb_image/stb_image.h>

#include <list>
#include <vector>
#include "Vertex.h"

using namespace glm;

struct LightSource
{ 
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

class Material
{
private:
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float shininess; 

	//In the Future lights that will affect my material
	std::list<LightSource*> Lights;

	//Each materials texture Location
	unsigned int texture;

private: 
	//Future implementation
	//Shader* lightShader;

	void setDiffuse(FILE* fPtr, std::vector<Vertex>& vertices, int offset);
	void setAmbient(FILE* fPtr, std::vector<Vertex>& vertices, int offset);
	void setSpecular(FILE* fPtr, std::vector<Vertex>& vertices, int offset);

public:

	Material();
	Material(vec3 Ambient, vec3 Diffuse, vec3 Spec);
	~Material();

	void UseTexture() { glBindTexture(GL_TEXTURE_2D, texture); }

	void loadMaterial(std::string name, std::vector<Vertex>& vertices, int offeset);

	void LoadTexture(char* path);

	unsigned int getTextureID() const { return texture; }
};

#endif // !MATERIAL_H

